
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;


public class Clase_rmi extends UnicastRemoteObject implements Interface_rmi {
    public Clase_rmi() throws RemoteException {
        super();
    }

    public double[][] multiplica_matriz(double[][] A, double[][] B) throws RemoteException {
        double[][] C = new double[A.length][B.length];
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < B.length; j++) {
                double suma = 0;
                for (int k = 0; k < A[i].length; k++)
                    suma = suma + A[i][k] * B[j][k];
                C[i][j] = suma;
            }
        }
        return C;
    }
}