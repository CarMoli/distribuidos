
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Interface_rmi extends Remote
{
    public double[][] multiplica_matriz(double[][] A, double[][] B) throws RemoteException;
}
