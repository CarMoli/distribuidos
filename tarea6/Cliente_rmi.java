import java.rmi.Naming;
import java.util.Arrays; 
public class Cliente_rmi {
    public static void main(String args[]) throws Exception {
        int N = 500;
        int aux = 0;
        double[][] A = new double[N][N];
        double[][] B = new double[N][N];
        double[][] BTranspuesta = new double[N][N];
        double[][] C = new double[N][N];
        double checksum = 0;

        Interface_rmi nodo1 = (Interface_rmi) Naming.lookup("rmi://10.242.130.107/prueba");
        Interface_rmi nodo2 = (Interface_rmi) Naming.lookup("rmi://10.242.130.85/prueba");
        Interface_rmi nodo3 = (Interface_rmi) Naming.lookup("rmi://10.242.130.52/prueba");
      
        double[][] DatosNodo = new double[N / 2][N];
        double[][] DatosTrans = new double[N / 2][N];
        double[][] Datosnodo1 = new double[N/2][N/2];
        double[][] Datosnodo2 = new double[N/2][N/2];
        double[][] Datosnodo3 = new double[N/2][N/2];

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                A[i][j] = 2 * i + j;
                B[i][j] = i * 2 - j;
            }

        //Transpuesta
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                BTranspuesta[i][j] = B[j][i];

        for (int i = 0; i < N / 2; i++) {
            for (int j = 0; j < N / 2; j++) {
                double suma = 0;
                for (int k = 0; k < N; k++)
                    suma = suma + A[i][k] * BTranspuesta[j][k];
                C[i][j] = suma;
            }
        }


        for (int i = 0; i < N / 2; i++) {
            for (int j = 0; j < N; j++) {
                DatosNodo[i][j] = A[i][j];
            }
        }

        for (int i = N / 2; i < N; i++) {
            for (int j = 0; j < N; j++) {
                DatosTrans[aux][j] = BTranspuesta[i][j];
            }
            aux++;
        }

        Datosnodo1 = nodo1.multiplica_matriz(DatosNodo, DatosTrans);
        aux = 0;
        for (int i = 0; i < N / 2; i++) {
            for (int j = N / 2; j < N; j++) {
                C[i][j] = Datosnodo1[i][aux];
                aux++;
            }
            aux = 0;
        }

        aux = 0;
        for (int i = N / 2; i < N; i++) {
            for (int j = 0; j < N; j++) {
                DatosNodo[aux][j] = A[i][j];
            }
            aux++;
        }
        aux = 0;
        for (int i = 0; i < N / 2; i++) {
            for (int j = 0; j < N; j++) {
                DatosTrans[i][aux] = BTranspuesta[i][j];
                aux++;
            }
            aux = 0;
        }

        Datosnodo2 = nodo2.multiplica_matriz(DatosNodo, DatosTrans);
        aux=0;
        for (int i = N / 2; i < N; i++) {
            for (int j = 0; j < N / 2; j++) {
                C[i][j] = Datosnodo2[aux][j];
            }
            aux++;
        }

        aux = 0;
        for (int i = N / 2; i < N; i++) {
            for (int j = 0; j < N; j++) {
                DatosNodo[aux][j] = A[i][j];
            }
            aux++;
        }
        aux = 0;
        for (int i = N / 2; i < N; i++) {
            for (int j = 0; j < N; j++) {
                DatosTrans[aux][j] = BTranspuesta[i][j];
            }
            aux++;
        }

        Datosnodo3 = nodo3.multiplica_matriz(DatosNodo, DatosTrans);  
        aux=0;      
        int aux1=0;
        for (int i = N / 2; i < N; i++) {
            for (int j = N / 2; j < N; j++) {
                C[i][j] = Datosnodo3[aux][aux1];
                aux1++;
            }
            aux1=0;
            aux++;
        }

        if (N <= 4) {
            System.out.println("Matriz A");
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    System.out.print(" | " + A[i][j] + " | ");
                }
                System.out.println();
            }

            System.out.println("Matriz B");
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    System.out.print(" | " + B[i][j] + " | ");
                }
                System.out.println();
            }

            System.out.println("Matriz C=AxB");
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    System.out.print(" | " + C[i][j] + " | ");
                }
                System.out.println();
            }
        } else {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    checksum += C[i][j];
                }
            }
            System.out.println(checksum);
        }

    }
}
