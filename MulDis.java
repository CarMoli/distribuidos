final static int N = 4;
double[][] A = new double[N][N];
double[][] B = new double[N][N];
double[][] C = new double[N][N];
//inicializar A y B
//transponer B

double[][] c1 = multiplica_matrices(copia_matriz(A, 0), copia_matriz(B, 0));
double[][] c2 = multiplica_matrices(copia_matriz(A, 0), copia_matriz(B, N/2));
double[][] c3 = multiplica_matrices(copia_matriz(A, N/2), copia_matriz(B, 0));
double[][] c4 = multiplica_matrices(copia_matriz(A, N/2), copia_matriz(B, N/2));

acomoda(C, c1, 0, 0);
acomoda(C, c2, 0, N/2);
acomoda(C, c3, N/2, 0);
acomoda(C, c4, N/2, N/2);

//desplegar la matriz C (N=4)
//calcular el checksum
