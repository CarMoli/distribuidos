import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.lang.Thread;
import java.lang.Math;
import java.nio.ByteBuffer;

class pi{

	static int n=0;
	static Object lock = new Object();
	static double pi=0;
	static int p=0,k=0;
	static double sumanodo0 = 0;
	static double sumanodo = 0;

	static class Worker extends Thread{
		Socket conexion;
		Worker (Socket conexion){
			this.conexion = conexion;
		}
		public void run(){
			try{
				// Declarar y crear los streams de entrada y salida.
				DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
        		DataInputStream entrada = new DataInputStream(conexion.getInputStream());
        		//----------------------------------------------------
        		//Recibir la suma calculada por el cliente.
        		double sumacliente = entrada.readInt();
        		//System.out.println(sumacliente);
        		//----------------------------------------------------

        		//En un bloque sincronizado mediante el objeto "lock":
				synchronized(lock){
					//System.out.println("bloque sincronizado mediante el objeto lock");
					//Sumar a la variable "pi" la suma obtenida
					pi = sumacliente + pi;
					//------------------------------------------------------------

					//Incrementar la variable "n"
					n++;
					//------------------------------------------------------------
				}
				//Cerrar la conexión y los streams de entrada y salida.
        		salida.close();
        		entrada.close();
        		//------------------------------------------------------

        		//Termina el thread
        		conexion.close();
        		//------------------------------------------------------

      			}

      		catch (Exception e)
      		{
        		System.err.println(e.getMessage());
      		}


		}
	}

  public static void main(String[] args)throws Exception{
	
	//Verificar que el número de nodo pase como parámetro al programa.
	if (args.length != 1){
		System.err.println("falta el número de nodo");
		System.exit(-1);
	}
	//----------------------------------------------------------------

	// Obtener el número de nodo que pasó como parámetro al programa.
	int nodo = Integer.valueOf(args[0]);
 	//----------------------------------------------------------------
	
	//Si el nodo actual es el nodo cero
	if (nodo == 0){
		// Crear el socket servidor (listener).
		ServerSocket socket = new ServerSocket(50000);
		//----------------------------------------------------------------
		//Esperar la conexión de 3 nodos dentro de un ciclo. Para cada conexión recibida crear un thread, pasando como parámetro al thread la conexión.
		for(int i=0;i<3;i++){
			Socket conexion = socket.accept();
			Worker w = new Worker(conexion);
			w.start();
		}
		//----------------------------------------------------------------

		//calcular la suma del nodo 0
		for(p=0,k=1000000;p<k;p++){
			sumanodo0 = (Math.pow(-1,p))*(4.0/(2.0*p+1.0)) + sumanodo0; 
		}
		//System.out.println("sumanodo0= " + sumanodo0);
		//----------------------------------------------------------------
		//En un bloque sincronizado mediante el objeto "lock":
		synchronized(lock){
			//System.out.println("bloque sincronizado mediante el objeto lock");
			//Sumar a la variable "pi" la suma obtenida
			pi = sumanodo0 + pi;
			//------------------------------------------------------------

			//Incrementar la variable "n"
			n++;
			//------------------------------------------------------------
		}
		//----------------------------------------------------------------

		//Esperar a que la variable "n" sea 4, sincronizando la lectura de "n" mediante el objeto "lock"
		synchronized(lock){
			if (n == 4){
				//Desplegar el valor de la variable "pi" multiplicado por cuatro
				System.out.println("pi= " + pi);
				//pi = pi*4;
				//System.out.println("pi * 4 = " + pi);
				//--------------------------------------------------------------
			}
		}
		//---------------------------------------------------------------------------------------------
	}
	//termina flujo principal.

	//Si el nodo actual es diferente del nodo cero:
	else{
		//Conectar con el servidor.
		Socket conexion = new Socket("localhost",50000);
		//----------------------------------------------------------------------

		//Declarar y crear los streams de entrada y salida.
		DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
    	DataInputStream entrada = new DataInputStream(conexion.getInputStream());
		//----------------------------------------------------------------------

		//Calcular la suma que corresponde al nodo.
    	switch(nodo){
    		case 1:
    			p=1000000;
    			k=2000000;
    		break;
    		case 2:
    			p=2000000;
    			k=3000000;
    		break;
    		case 3:
    			p=3000000;
    			k=4000000;
    		break;
    		default:
    			p=0;
    			k=1;
    		break;
    	}

		for(int i=p;i<k;i++){
			sumanodo = (Math.pow(-1,p))*(4.0/(2.0*p+1.0)) + sumanodo; 
		}

		//System.out.println("sumanodo= " + sumanodo);
		//----------------------------------------------------------------------

		//Enviar la suma al servidor
		salida.writeDouble(sumanodo);
		//----------------------------------------------------------------------

		//Cerrar la conexión y los streams de entrada y salida.
        salida.close();
        entrada.close();
        //------------------------------------------------------

        //Termina el thread
        conexion.close();
        //------------------------------------------------------

        //termina flujo.
	}
  }
}