import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.lang.Thread;
import java.nio.ByteBuffer;

public class tarea1 {
    static int N = 4;
    static double A[][];
    static double B[][];                    
    static double C[][];
    static int n = 0;   
    static Object lock = new Object(); 
    
    static class Worker extends Thread{
	    Socket conexion;
	
        Worker(Socket conexion){
            this.conexion = conexion;
	    }
        
	    public void run() {
            try{
                DataInputStream entrada = new DataInputStream(conexion.getInputStream());
                DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
                int nodo = entrada.readInt();
                if(nodo == 1){  
                    //Envia al cliente los renglones 0 al N/2-1 de la matriz A y las columnas N/2 al N-1 de la matriz
                    for(int i = 0; i < N/2; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(A[i][j]);
                        }
                    }
                    for(int i = 0; i < N; i++){
                        for(int j = N/2; j < N; j++){
                            salida.writeDouble(B[i][j]);
                        }
                    }
                    //Recibe del cliente el cuadrante superior derecho de la matriz C
                    for(int i = 0; i < N/2; i++){
                        for(int j = N/2; j < N; j++){
                            C[i][j] = entrada.readDouble();
                        }
                    }
                } else if(nodo == 2){   
                    //Envia al cliente los renglones N/2 al N-1 de la matriz A y los renglones 0 al N/2-1 de la matriz B.
                    for(int i = N/2; i < N; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(A[i][j]);
                        }
                    }
                    for(int i = 0; i < N; i++){
                        for(int j = 0; j < N/2; j++){
                            salida.writeDouble(B[i][j]);
                        }
                    }
                    //Recibe del cliente el cuadrante inferior izquierdo de la matriz C.
                    for(int i = N/2; i < N; i++){
                        for(int j = 0; j < N/2; j++){
                            C[i][j] = entrada.readDouble();
                        }
                    }
                }else if(nodo == 3){    //12
                    //Envia al cliente los renglones N/2 al N-1 de la matriz A y los renglones N/2 al N-1 de la matriz B.
                    for(int i = N/2; i < N; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(A[i][j]);
                        }
                    }
                    for(int i = 0; i < N; i++){
                        for(int j = N/2; j < N; j++){
                            salida.writeDouble(B[i][j]);
                        }
                    }
                    //Recibe del cliente el cuadrante inferior derecho de la matriz C.
                    for(int i = N/2; i < N; i++){
                        for(int j = N/2; j < N; j++){
                            C[i][j]=entrada.readDouble();
                        }
                    }
                }
                //Incrementa la variable "n" sincronizando mediante el objeto "lock".
                synchronized(lock){
                    n++;
                }
                // Cierra la conexión y los streams de entrada y salida.
                salida.close();
                entrada.close();
                conexion.close();
            } catch (Exception e){
                System.err.println(e.getMessage());
            }
        }
    }
    
    public static void main(String[] args) throws Exception {
	// Verifica que el número de nodo pase como parámetro al programa.
        if(args.length != 1){
            System.err.println("Se espera el numero de nodo");
            System.exit(-1);
	    }
	    int nodo = Integer.valueOf(args[0]);
        if(nodo == 0){                       
            A = new double[N][N];
            B = new double[N][N];
            C = new double[N][N];
            //7.2           
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    A[i][j] = 2 * i + j;
                }
            }
            //Omite la parte de la transpuesta ya que para mi es mas facil verlo de la siguiente forma
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    B[i][j] = 2 * i - j;
                }
            }
            ServerSocket servidor = new ServerSocket(50000); 
            // Espera la conexión de 3 nodos dentro de un ciclo
            for (int i=0; i<3; i++) {
                Socket conexion = servidor.accept();
		        Worker w = new Worker(conexion);
		        w.start();
            }
            //Calcula el cuadrante superior izquierdo de la matriz C
            for(int i = 0; i < N/2; i++){
                for(int j = 0; j < N/2; j++){
                    for(int k = 0; k < N; k++){
                        C[i][j] += A[i][k] * B[k][j]; 
                    }
                }
            }
            //Incrementa la variable "n" sincronizando mediante el objeto "lock"
            synchronized(lock){
		        n++;
            }
            // Espera a que la variable "n" sea 4
            for (;;) {
		        synchronized(lock){
                if(n == 4)
                    break;
		        }
                Thread.sleep(100);
            }
            // Muestra resultados
            System.out.println("NUEVO Resultado");
            System.out.println("Matriz A");
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    System.out.print(A[i][j] + "\t");
                }
                System.out.println();
            }
            System.out.println("Matriz B");
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    System.out.print(B[i][j] + "\t");
                }
                System.out.println();
            }
            System.out.println("Matriz C");
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    System.out.print(C[i][j] + "\t");
                }
                System.out.println();
            }
            //Calcula el checksum de la matriz C y desplegarlo
            double sum = 0;
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    sum += C[i][j];
                }
            }
            System.out.println("Checksum: " + sum);
        }else{ 
            A = new double[N/2][N];    
            B = new double[N][N/2];    
            C = new double[N/2][N/2];
            Socket conexion = new Socket("localhost", 50000);
            DataInputStream entrada = new DataInputStream(conexion.getInputStream());
            DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
            salida.writeInt(nodo);   
            //Recibe del servidor los renglones 0 al N/2-1 de la matriz A y los renglones del 0 al N/2-1 de la matriz B
            for(int i = 0; i < N/2; i++){
                for(int j = 0; j < N; j++){
                    A[i][j] = entrada.readDouble();
                }
            }
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N/2; j++){
                    B[i][j] = entrada.readDouble();
                }
            }
            // Calcula la matriz C (N/2 renglones y N/2 columnas).
            for(int i = 0; i < N/2; i++){
                for(int j = 0; j < N/2; j++){
                    for(int k = 0; k < N; k++){
                        C[i][j] += A[i][k] * B[k][j]; 
                    }
                }
            }
            // Envia al servidor la matriz C.
            for(int i = 0; i < N/2; i++){
                for(int j = 0; j < N/2; j++){
                    salida.writeDouble(C[i][j]);
                }
            }
            // Cierra la conexion y los streams de entrada y salida.
            salida.close();
            entrada.close();
        } 
    }
}
